﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Trabajo_semestral.Models;

namespace Trabajo_semestral.Controllers
{
    public class JugadoresController : Controller
    {
        private FutGolEntities db = new FutGolEntities();

        // GET: Jugadores
        public ActionResult Index()
        {
            var query = (from n in db.Equipos
                         select n.nombre
                        ).Distinct()
                        .OrderBy(nombre => nombre);
            var jugadores = db.Jugadores.Include(j => j.Equipos);
            ViewBag.filtro_equipos = new SelectList(query);
            return View(jugadores.ToList());
        }

        //POST: Jugadores
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string filtro_equipos)
        {
            var query = (from n in db.Equipos
                         select n.nombre
                        ).Distinct()
                        .OrderBy(nombre => nombre);
            var jugadores = db.Jugadores.Include(j => j.Equipos);
            ViewBag.filtro_equipos = new SelectList(query);
            return View(jugadores.Where(x => x.Equipos.nombre == filtro_equipos).ToList());
        }

        // GET: Jugadores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Jugadores jugadores = db.Jugadores.Find(id);
            if (jugadores == null)
            {
                return HttpNotFound();
            }
            return View(jugadores);
        }

        // GET: Jugadores/Create
        public ActionResult Create()
        {
            ViewBag.id_equipo = new SelectList(db.Equipos, "id", "nombre");
            return View();
        }

        // POST: Jugadores/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,nombres,apellidos,posicion,id_equipo")] Jugadores jugadores)
        {

            if (jugadores.nombres.Length < 3)
            {
                return View(jugadores);
            }

            if (jugadores.apellidos.Length < 3)
            {
                return View(jugadores);
            }


            if (ModelState.IsValid)
            {
                db.Jugadores.Add(jugadores);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_equipo = new SelectList(db.Equipos, "id", "nombre", jugadores.id_equipo);
            return View(jugadores);
        }

        // GET: Jugadores/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Jugadores jugadores = db.Jugadores.Find(id);
            if (jugadores == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_equipo = new SelectList(db.Equipos, "id", "nombre", jugadores.id_equipo);
            return View(jugadores);
        }

        // POST: Jugadores/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,nombres,apellidos,posicion,id_equipo")] Jugadores jugadores)
        {
            if (ModelState.IsValid)
            {
                db.Entry(jugadores).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_equipo = new SelectList(db.Equipos, "id", "nombre", jugadores.id_equipo);
            return View(jugadores);
        }

        // GET: Jugadores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Jugadores jugadores = db.Jugadores.Find(id);
            if (jugadores == null)
            {
                return HttpNotFound();
            }
            return View(jugadores);
        }

        // POST: Jugadores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Jugadores jugadores = db.Jugadores.Find(id);
            db.Jugadores.Remove(jugadores);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
